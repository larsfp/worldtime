#!/usr/bin/env python3

import datetime
import pytz
import os
import sys
from rich import print as rprint
from rich.console import Console
from rich.layout import Layout
from rich.panel import Panel
from rich import box
from rich.console import Console
from rich.table import Table
import holidays
from pyfiglet import Figlet

# Icons
icon_night = "💤"
icon_weekend = "📅"
icon_day = "🌞"
icon_office = "🏢"
icon_holyday = "🎉"

class Cities:
  def __init__(self, name, holidaylist, timezone):
    self.name = name
    self.holidaylist = holidaylist
    self.timezone = timezone


city_list = []

#user config
city_list.append(Cities(None, holidays.Norway(), "Europe/Oslo"))
city_list.append(Cities(None, holidays.Serbia(), "Europe/Belgrade"))
city_list.append(Cities("Buenos Aires", holidays.Argentina(), "America/Argentina/Buenos_Aires"))
city_list.append(Cities("Mexico", holidays.Mexico(), "America/Mexico_City"))
city_list.append(Cities("Puerto", holidays.UnitedStates(), "America/Los_Angeles"))
city_list.append(Cities("Cebu", holidays.Norway(), "Asia/Manila")) # TODO:!

def generate_hours():
    hours = list(range(hour, -1, -1))
    if hour != 23:
        hours += list(range(23, hour, -1))
    return hours

def generate_timeline():
    output = ''
    for h in hours:
        output += "%02d " % h
    return output

def generate_officehours():
    output = ''
    officehours = list(range(8, 17))
    for h in hours:
        if h in officehours:
            output += f"{icon_office} "
        else:
            output += f"{icon_night} "
    return output

def format_city(city_timezone, city_holidaylist, city_name, timeline, home = False):
    icon = icon_night
    if not city_name:
        city_name = city_timezone.split('/')[-1]
    timezone = pytz.timezone(city_timezone)
    date = datetime.datetime.now()
    if home:
        city_name = "[bold magenta]" + city_name + "[/bold magenta]"

    date_in_city = date.astimezone(timezone)
    hour_in_city = date_in_city.hour

    offset = timeline.index("%02d" % hour_in_city)
    if is_holiday(date_in_city, city_holidaylist):
        icon = icon_holyday
    if is_weekend(date_in_city):
        icon = icon_weekend
    elif hour_in_city in list((range(8, 17))):
        icon = icon_day

    # {date_in_city.strftime('%H:%M')}
    return f"{''.ljust(offset)}{icon} {city_name}\n"

def hide_cursor():
    os.system('setterm -cursor off')

def show_cursor():
    os.system('setterm -cursor on')

def is_weekend(d):
      return d.weekday() > 4

def is_holiday(date_in_city, local_holidays):
      return date_in_city in local_holidays


#system
hour = datetime.datetime.now().hour
hours = generate_hours()
columns, rows = os.get_terminal_size(0)
timeline = generate_timeline()

f = Figlet(font='6x10', justify='center')
clock = datetime.datetime.now().strftime("%H:%M")
rprint("[bold magenta]" + f.renderText(clock))

city_output = ''
for city in city_list:
    city_output += format_city(
        city.timezone, 
        city.holidaylist, 
        city.name, 
        timeline, 
        home=(city == city_list[0])
    )

table = Table(title="Copyleft World Time")
table.add_column(timeline + "\n" + generate_officehours(), no_wrap=True)
table.add_row(city_output)

console = Console()
console.print(table)

print ("""Legend:
* icon_night \t= "💤"
* icon_weekend \t= "📅"
* icon_day \t= "🌞"
* icon_office \t= "🏢"
* icon_holyday \t= "🎉"
""")
